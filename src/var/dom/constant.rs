use contrail::Trail;

use crate::var::DomInt;
use crate::var::Domain;
use crate::var::Event;

#[derive(Clone, Copy)]
pub struct ConstantDom {
    const_val: DomInt,
}

impl ConstantDom {
    pub fn new(const_val: DomInt) -> Self {
        Self { const_val }
    }
}

impl Domain for ConstantDom {
    #[inline(always)]
    fn size(&self, _trail: &Trail) -> usize {
        1
    }

    #[inline(always)]
    fn val(&self, _trail: &Trail) -> Option<DomInt> {
        Some(self.const_val)
    }

    #[inline(always)]
    fn min(&self, _trail: &Trail) -> DomInt {
        self.const_val
    }

    #[inline(always)]
    fn max(&self, _trail: &Trail) -> DomInt {
        self.const_val
    }

    #[inline(always)]
    fn contains(&self, _trail: &Trail, val: DomInt) -> bool {
        val == self.const_val
    }

    #[inline(always)]
    fn assign(&self, _trail: &mut Trail, val: DomInt) -> Result<Event, ()> {
        if val == self.const_val {
            Ok(Event::None)
        } else {
            Err(())
        }
    }

    #[inline(always)]
    fn remove(&self, _trail: &mut Trail, val: DomInt) -> Result<Event, ()> {
        if val != self.const_val {
            Ok(Event::None)
        } else {
            Err(())
        }
    }

    #[inline(always)]
    fn update_min(&self, _trail: &mut Trail, new_min: DomInt) -> Result<Event, ()> {
        if new_min <= self.const_val {
            Ok(Event::None)
        } else {
            Err(())
        }
    }

    #[inline(always)]
    fn update_max(&self, _trail: &mut Trail, new_max: DomInt) -> Result<Event, ()> {
        if new_max >= self.const_val {
            Ok(Event::None)
        } else {
            Err(())
        }
    }
}
