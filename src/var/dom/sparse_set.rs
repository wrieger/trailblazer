use crate::var::DomInt;
use crate::var::Domain;
use crate::var::Event;

#[derive(Clone, Copy)]
pub struct SparseSetDom {
    min: contrail::TrailedI64,
    max: contrail::TrailedI64,
    offset: i64,
    values: contrail::collections::TrailedSparseSet,
}

impl std::fmt::Debug for SparseSetDom {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "asdf")
    }
}

impl SparseSetDom {
    pub fn from_bounds(builder: &mut contrail::TrailBuilder, min: DomInt, max: DomInt) -> Self {
        Self {
            min: contrail::TrailedI64::new(builder, min.0),
            max: contrail::TrailedI64::new(builder, max.0),
            offset: min.0,
            values: contrail::collections::TrailedSparseSet::new_full(
                builder,
                (max.0 - min.0 + 1) as usize,
            ),
        }
    }
}

impl Domain for SparseSetDom {
    #[inline(always)]
    fn size(&self, trail: &contrail::Trail) -> usize {
        self.values.len(trail)
    }

    #[inline(always)]
    fn contains(&self, trail: &contrail::Trail, val: DomInt) -> bool {
        let min = self.min.get(&trail);
        if min <= val.0 {
            self.values.contains(
                trail,
                contrail::collections::Value((val.0 - self.offset) as usize),
            )
        } else {
            false
        }
    }

    #[inline(always)]
    fn val(&self, trail: &contrail::Trail) -> Option<DomInt> {
        if self.values.len(trail) == 1 {
            Some(DomInt(self.min.get(trail)))
        } else {
            None
        }
    }

    #[inline(always)]
    fn min(&self, trail: &contrail::Trail) -> DomInt {
        DomInt(self.min.get(trail))
    }

    #[inline(always)]
    fn max(&self, trail: &contrail::Trail) -> DomInt {
        DomInt(self.max.get(trail))
    }

    fn assign(&self, trail: &mut contrail::Trail, val: DomInt) -> Result<Event, ()> {
        if !self.contains(trail, val) {
            Err(())
        } else {
            if self.values.len(trail) == 1 {
                Ok(Event::None)
            } else {
                self.values.intersect(
                    trail,
                    std::iter::once(contrail::collections::Value((val.0 - self.offset) as usize)),
                );
                self.min.set(trail, val.0);
                self.max.set(trail, val.0);
                Ok(Event::Assigned)
            }
        }
    }

    fn remove(&self, trail: &mut contrail::Trail, val: DomInt) -> Result<Event, ()> {
        if !self.contains(trail, val) {
            Ok(Event::None)
        } else {
            let mut min = self.min.get(trail);
            let mut max = self.max.get(trail);
            if self.values.len(trail) == 1 {
                Err(())
            } else {
                self.values.remove(
                    trail,
                    contrail::collections::Value((val.0 - self.offset) as usize),
                );
                if self.values.len(trail) == 1 {
                    if val.0 == min {
                        self.min.set(trail, max);
                    } else {
                        self.max.set(trail, min);
                    }
                    Ok(Event::Assigned)
                } else if val.0 == min {
                    while !self.contains(trail, DomInt(min)) {
                        min += 1;
                    }
                    self.min.set(trail, min);
                    if min == max {
                        Ok(Event::Assigned)
                    } else {
                        Ok(Event::BoundsChanged)
                    }
                } else if val.0 == max {
                    while !self.contains(trail, DomInt(max)) {
                        max -= 1;
                    }
                    self.max.set(trail, max);
                    if min == max {
                        Ok(Event::Assigned)
                    } else {
                        Ok(Event::BoundsChanged)
                    }
                } else {
                    Ok(Event::Reduced)
                }
            }
        }
    }

    fn update_min(&self, trail: &mut contrail::Trail, new_min: DomInt) -> Result<Event, ()> {
        let max = self.max.get(trail);
        let mut min = self.min.get(trail);
        if new_min.0 > max {
            Err(())
        } else if new_min.0 == max {
            if min == new_min.0 {
                Ok(Event::None)
            } else {
                self.min.set(trail, max);
                self.values.intersect(
                    trail,
                    std::iter::once(contrail::collections::Value((max - self.offset) as usize)),
                );
                Ok(Event::Assigned)
            }
        } else {
            if new_min.0 <= min {
                Ok(Event::None)
            } else {
                for val in (min..new_min.0)
                    .map(|i| contrail::collections::Value((i - self.offset) as usize))
                {
                    self.values.remove(trail, val);
                }
                min = new_min.0;
                while !self.contains(trail, DomInt(min)) {
                    min += 1;
                }
                self.min.set(trail, min);
                if min == max {
                    Ok(Event::Assigned)
                } else {
                    Ok(Event::BoundsChanged)
                }
            }
        }
    }

    fn update_max(&self, trail: &mut contrail::Trail, new_max: DomInt) -> Result<Event, ()> {
        let min = self.min.get(trail);
        let mut max = self.max.get(trail);
        if new_max.0 < min {
            Err(())
        } else if new_max.0 == min {
            if max == new_max.0 {
                Ok(Event::None)
            } else {
                self.max.set(trail, min);
                self.values.intersect(
                    trail,
                    std::iter::once(contrail::collections::Value((min - self.offset) as usize)),
                );
                Ok(Event::Assigned)
            }
        } else {
            if new_max.0 >= max {
                Ok(Event::None)
            } else {
                for val in ((new_max.0 + 1)..=max)
                    .map(|i| contrail::collections::Value((i - self.offset) as usize))
                {
                    self.values.remove(trail, val);
                }
                max = new_max.0;
                while !self.contains(trail, DomInt(max)) {
                    max -= 1;
                }
                self.max.set(trail, max);
                if max == min {
                    Ok(Event::Assigned)
                } else {
                    Ok(Event::BoundsChanged)
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sparse_int_dom_update_min() {
        let mut trail = contrail::Trail::new();
        let dom = SparseSetDom::from_bounds(&mut trail, DomInt(0), DomInt(4));

        trail.new_level();

        assert_eq!(dom.update_min(&mut trail, DomInt(-1)), Ok(Event::None));
        assert_eq!(dom.update_min(&mut trail, DomInt(0)), Ok(Event::None));
        assert_eq!(
            dom.update_min(&mut trail, DomInt(1)),
            Ok(Event::BoundsChanged)
        );
        assert_eq!(dom.min(&trail), DomInt(1));

        trail.new_level();

        assert_eq!(
            dom.update_min(&mut trail, DomInt(3)),
            Ok(Event::BoundsChanged)
        );
        assert_eq!(dom.min(&trail), DomInt(3));

        trail.new_level();

        assert_eq!(dom.update_min(&mut trail, DomInt(4)), Ok(Event::Assigned));
        assert_eq!(dom.min(&trail), DomInt(4));

        trail.new_level();

        assert_eq!(dom.update_min(&mut trail, DomInt(5)), Err(()));

        trail.undo_level();

        assert_eq!(dom.min(&trail), DomInt(4));

        trail.undo_level();

        assert_eq!(dom.min(&trail), DomInt(3));

        trail.undo_level();

        assert_eq!(dom.min(&trail), DomInt(1));

        trail.undo_level();

        assert_eq!(dom.min(&trail), DomInt(0));
    }

    #[test]
    fn sparse_int_dom_update_max() {
        let mut trail = contrail::Trail::new();
        let dom = SparseSetDom::from_bounds(&mut trail, DomInt(0), DomInt(4));

        trail.new_level();

        assert_eq!(dom.update_max(&mut trail, DomInt(5)), Ok(Event::None));
        assert_eq!(dom.update_max(&mut trail, DomInt(4)), Ok(Event::None));
        assert_eq!(
            dom.update_max(&mut trail, DomInt(3)),
            Ok(Event::BoundsChanged)
        );
        assert_eq!(dom.max(&trail), DomInt(3));

        trail.new_level();

        assert_eq!(
            dom.update_max(&mut trail, DomInt(1)),
            Ok(Event::BoundsChanged)
        );
        assert_eq!(dom.max(&trail), DomInt(1));

        trail.new_level();

        assert_eq!(dom.update_max(&mut trail, DomInt(0)), Ok(Event::Assigned));
        assert_eq!(dom.max(&trail), DomInt(0));

        trail.new_level();

        assert_eq!(dom.update_max(&mut trail, DomInt(-1)), Err(()));

        trail.undo_level();

        assert_eq!(dom.max(&trail), DomInt(0));

        trail.undo_level();

        assert_eq!(dom.max(&trail), DomInt(1));

        trail.undo_level();

        assert_eq!(dom.max(&trail), DomInt(3));

        trail.undo_level();

        assert_eq!(dom.max(&trail), DomInt(4));
    }

    #[test]
    fn sparse_int_dom_remove() {
        let mut trail = contrail::Trail::new();
        let dom = SparseSetDom::from_bounds(&mut trail, DomInt(0), DomInt(4));

        trail.new_level();

        assert_eq!(dom.remove(&mut trail, DomInt(4)), Ok(Event::BoundsChanged));
        assert_eq!(dom.remove(&mut trail, DomInt(1)), Ok(Event::Reduced));
        assert_eq!(dom.remove(&mut trail, DomInt(0)), Ok(Event::BoundsChanged));
        assert_eq!(dom.min(&trail), DomInt(2));
        assert_eq!(dom.max(&trail), DomInt(3));

        trail.new_level();

        assert_eq!(dom.remove(&mut trail, DomInt(3)), Ok(Event::Assigned));

        trail.undo_level();

        assert!(dom.contains(&trail, DomInt(3)));

        trail.undo_level();

        assert!(dom.contains(&trail, DomInt(0)));
        assert!(dom.contains(&trail, DomInt(1)));
        assert!(dom.contains(&trail, DomInt(4)));
    }
}
