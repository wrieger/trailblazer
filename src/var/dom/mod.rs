mod bit_set;
mod constant;
mod dual;
// mod sparse_set;
mod zero_one;

pub use self::bit_set::BitSetDom;
pub use self::constant::ConstantDom;
pub use self::dual::DualDom;
pub use self::dual::MixedDom2;
pub use self::dual::MixedDom3;
pub use self::dual::MixedDom4;
// pub use self::sparse_set::SparseSetDom;
pub use self::zero_one::ZeroOneDom;
pub use self::zero_one::ZeroOneDomArray;

pub fn mixed2_0<D0, D1>(dom0: D0) -> MixedDom2<D0, D1> {
    DualDom::Dom0(dom0)
}

pub fn mixed2_1<D0, D1>(dom1: D1) -> MixedDom2<D0, D1> {
    DualDom::Dom1(dom1)
}

pub fn mixed3_0<D0, D1, D2>(dom0: D0) -> MixedDom3<D0, D1, D2> {
    DualDom::Dom0(DualDom::Dom0(dom0))
}

pub fn mixed3_1<D0, D1, D2>(dom1: D1) -> MixedDom3<D0, D1, D2> {
    DualDom::Dom0(DualDom::Dom1(dom1))
}

pub fn mixed3_2<D0, D1, D2>(dom2: D2) -> MixedDom3<D0, D1, D2> {
    DualDom::Dom1(dom2)
}

pub fn mixed4_0<D0, D1, D2, D3>(dom0: D0) -> MixedDom4<D0, D1, D2, D3> {
    DualDom::Dom0(DualDom::Dom0(DualDom::Dom0(dom0)))
}

pub fn mixed4_1<D0, D1, D2, D3>(dom1: D1) -> MixedDom4<D0, D1, D2, D3> {
    DualDom::Dom0(DualDom::Dom0(DualDom::Dom1(dom1)))
}

pub fn mixed4_2<D0, D1, D2, D3>(dom2: D2) -> MixedDom4<D0, D1, D2, D3> {
    DualDom::Dom0(DualDom::Dom1(dom2))
}

pub fn mixed4_3<D0, D1, D2, D3>(dom3: D3) -> MixedDom4<D0, D1, D2, D3> {
    DualDom::Dom1(dom3)
}
