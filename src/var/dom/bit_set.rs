use crate::var::DomInt;
use crate::var::Domain;
use crate::var::Event;

#[derive(Clone, Copy)]
pub struct BitSetDom {
    min: contrail::TrailedValue<i64>,
    max: contrail::TrailedValue<i64>,
    len: contrail::TrailedValue<usize>,
    offset: i64,
    values: contrail::collections::TrailedBitSet,
}

impl std::fmt::Debug for BitSetDom {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "asdf")
    }
}

impl BitSetDom {
    pub fn from_bounds(builder: &mut contrail::TrailBuilder, min: DomInt, max: DomInt) -> Self {
        assert!(max >= min);
        let len = (max.0 - min.0 + 1) as usize;
        Self {
            min: contrail::TrailedValue::new(builder, min.0),
            max: contrail::TrailedValue::new(builder, max.0),
            len: contrail::TrailedValue::new(builder, len),
            offset: min.0,
            values: contrail::collections::TrailedBitSet::new_full(
                builder,
                len
            ),
        }
    }
}

impl Domain for BitSetDom {
    #[inline(always)]
    fn size(&self, trail: &contrail::Trail) -> usize {
        self.len.get(trail)
    }

    #[inline(always)]
    fn contains(&self, trail: &contrail::Trail, val: DomInt) -> bool {
        let min = self.min.get(&trail);
        let max = self.max.get(&trail);
        if min <= val.0 && val.0 <= max {
            self.values.contains(trail, (val.0 - self.offset) as usize)
        } else {
            false
        }
    }

    #[inline(always)]
    fn val(&self, trail: &contrail::Trail) -> Option<DomInt> {
        let min = self.min.get(trail);
        let max = self.max.get(trail);
        if min == max {
            Some(DomInt(self.min.get(trail)))
        } else {
            None
        }
    }

    #[inline(always)]
    fn min(&self, trail: &contrail::Trail) -> DomInt {
        DomInt(self.min.get(trail))
    }

    #[inline(always)]
    fn max(&self, trail: &contrail::Trail) -> DomInt {
        DomInt(self.max.get(trail))
    }

    fn assign(&self, trail: &mut contrail::Trail, val: DomInt) -> Result<Event, ()> {
        if !self.contains(trail, val) {
            Err(())
        } else {
            if self.val(trail).is_some() {
                Ok(Event::None)
            } else {
                self.min.set(trail, val.0);
                self.max.set(trail, val.0);
                self.len.set(trail, 1);
                Ok(Event::Assigned)
            }
        }
    }

    fn remove(&self, trail: &mut contrail::Trail, val: DomInt) -> Result<Event, ()> {
        if !self.contains(trail, val) {
            Ok(Event::None)
        } else {
            if self.val(trail).is_some() {
                Err(())
            } else {
                let min = self.min.get(trail);
                let max = self.max.get(trail);
                self.values.remove(trail, (val.0 - self.offset) as usize);
                self.len.set(trail, self.len.get(trail) - 1);
                if self.len.get(trail) == 1 {
                    if val.0 == min {
                        self.min.set(trail, max);
                    } else {
                        self.max.set(trail, min);
                    }
                    Ok(Event::Assigned)
                } else if val.0 == min {
                    self.min.set(
                        trail,
                        self.values
                            .next_above(trail, (min - self.offset) as usize)
                            .unwrap() as i64
                            + self.offset,
                    );
                    if self.min.get(trail) == max {
                        Ok(Event::Assigned)
                    } else {
                        Ok(Event::BoundsChanged)
                    }
                } else if val.0 == max {
                    self.max.set(
                        trail,
                        self.values
                            .next_below(trail, (max - self.offset) as usize)
                            .unwrap() as i64
                            + self.offset,
                    );
                    if min == self.max.get(trail) {
                        Ok(Event::Assigned)
                    } else {
                        Ok(Event::BoundsChanged)
                    }
                } else {
                    Ok(Event::Reduced)
                }
            }
        }
    }

    fn update_min(&self, trail: &mut contrail::Trail, new_min: DomInt) -> Result<Event, ()> {
        let max = self.max.get(trail);
        let min = self.min.get(trail);
        if new_min.0 > max {
            Err(())
        } else if new_min.0 == max {
            if min == new_min.0 {
                Ok(Event::None)
            } else {
                self.len.set(trail, 1);
                self.min.set(trail, max);
                Ok(Event::Assigned)
            }
        } else {
            if new_min.0 <= min {
                Ok(Event::None)
            } else {
                for val in (min..new_min.0).map(|i| (i - self.offset) as usize) {
                    self.values.remove(trail, val);
                }
                self.min.set(
                    trail,
                    self.values
                        .next_above(trail, (new_min.0 - self.offset) as usize)
                        .unwrap() as i64
                        + self.offset,
                );
                self.len.set(
                    trail,
                    self.values.count_between(
                        trail,
                        (self.min.get(trail) - self.offset) as usize,
                        (self.max.get(trail) - self.offset) as usize,
                    ) as usize,
                );
                if self.len.get(trail) == 1 {
                    Ok(Event::Assigned)
                } else {
                    Ok(Event::BoundsChanged)
                }
            }
        }
    }

    fn update_max(&self, trail: &mut contrail::Trail, new_max: DomInt) -> Result<Event, ()> {
        let min = self.min.get(trail);
        let max = self.max.get(trail);
        if new_max.0 < min {
            Err(())
        } else if new_max.0 == min {
            if max == new_max.0 {
                Ok(Event::None)
            } else {
                self.max.set(trail, min);
                self.len.set(trail, 1);
                Ok(Event::Assigned)
            }
        } else {
            if new_max.0 >= max {
                Ok(Event::None)
            } else {
                for val in ((new_max.0 + 1)..=max).map(|i| (i - self.offset) as usize) {
                    self.values.remove(trail, val);
                }
                self.max.set(
                    trail,
                    self.values
                        .next_below(trail, (new_max.0 - self.offset) as usize)
                        .unwrap() as i64
                        + self.offset,
                );
                self.len.set(
                    trail,
                    self.values.count_between(
                        trail,
                        (self.min.get(trail) - self.offset) as usize,
                        (self.max.get(trail) - self.offset) as usize,
                    ) as usize,
                );
                if self.len.get(trail) == 1 {
                    Ok(Event::Assigned)
                } else {
                    Ok(Event::BoundsChanged)
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sparse_int_dom_update_min() {
        let mut builder = contrail::TrailBuilder::new();
        let dom = BitSetDom::from_bounds(&mut builder, DomInt(0), DomInt(4));
        let mut trail = builder.finish();

        trail.new_level();

        assert_eq!(dom.update_min(&mut trail, DomInt(-1)), Ok(Event::None));
        assert_eq!(dom.update_min(&mut trail, DomInt(0)), Ok(Event::None));
        assert_eq!(
            dom.update_min(&mut trail, DomInt(1)),
            Ok(Event::BoundsChanged)
        );
        assert_eq!(dom.min(&trail), DomInt(1));

        trail.new_level();

        assert_eq!(
            dom.update_min(&mut trail, DomInt(3)),
            Ok(Event::BoundsChanged)
        );
        assert_eq!(dom.min(&trail), DomInt(3));

        trail.new_level();

        assert_eq!(dom.update_min(&mut trail, DomInt(4)), Ok(Event::Assigned));
        assert_eq!(dom.min(&trail), DomInt(4));

        trail.new_level();

        assert_eq!(dom.update_min(&mut trail, DomInt(5)), Err(()));

        trail.undo_level();

        assert_eq!(dom.min(&trail), DomInt(4));

        trail.undo_level();

        assert_eq!(dom.min(&trail), DomInt(3));

        trail.undo_level();

        assert_eq!(dom.min(&trail), DomInt(1));

        trail.undo_level();

        assert_eq!(dom.min(&trail), DomInt(0));
    }

    #[test]
    fn sparse_int_dom_update_max() {
        let mut builder = contrail::TrailBuilder::new();
        let dom = BitSetDom::from_bounds(&mut builder, DomInt(0), DomInt(4));
        let mut trail = builder.finish();

        trail.new_level();

        assert_eq!(dom.update_max(&mut trail, DomInt(5)), Ok(Event::None));
        assert_eq!(dom.update_max(&mut trail, DomInt(4)), Ok(Event::None));
        assert_eq!(
            dom.update_max(&mut trail, DomInt(3)),
            Ok(Event::BoundsChanged)
        );
        assert_eq!(dom.max(&trail), DomInt(3));

        trail.new_level();

        assert_eq!(
            dom.update_max(&mut trail, DomInt(1)),
            Ok(Event::BoundsChanged)
        );
        assert_eq!(dom.max(&trail), DomInt(1));

        trail.new_level();

        assert_eq!(dom.update_max(&mut trail, DomInt(0)), Ok(Event::Assigned));
        assert_eq!(dom.max(&trail), DomInt(0));

        trail.new_level();

        assert_eq!(dom.update_max(&mut trail, DomInt(-1)), Err(()));

        trail.undo_level();

        assert_eq!(dom.max(&trail), DomInt(0));

        trail.undo_level();

        assert_eq!(dom.max(&trail), DomInt(1));

        trail.undo_level();

        assert_eq!(dom.max(&trail), DomInt(3));

        trail.undo_level();

        assert_eq!(dom.max(&trail), DomInt(4));
    }

    #[test]
    fn sparse_int_dom_remove() {
        let mut builder = contrail::TrailBuilder::new();
        let dom = BitSetDom::from_bounds(&mut builder, DomInt(0), DomInt(4));
        let mut trail = builder.finish();

        trail.new_level();

        assert_eq!(dom.remove(&mut trail, DomInt(4)), Ok(Event::BoundsChanged));
        assert_eq!(dom.remove(&mut trail, DomInt(1)), Ok(Event::Reduced));
        assert_eq!(dom.remove(&mut trail, DomInt(0)), Ok(Event::BoundsChanged));
        assert_eq!(dom.min(&trail), DomInt(2));
        assert_eq!(dom.max(&trail), DomInt(3));

        trail.new_level();

        assert_eq!(dom.remove(&mut trail, DomInt(3)), Ok(Event::Assigned));

        trail.undo_level();

        assert!(dom.contains(&trail, DomInt(3)));

        trail.undo_level();

        assert!(dom.contains(&trail, DomInt(0)));
        assert!(dom.contains(&trail, DomInt(1)));
        assert!(dom.contains(&trail, DomInt(4)));
    }
}
