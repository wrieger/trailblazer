use crate::var::DomInt;
use crate::var::Domain;
use crate::var::Event;

pub type MixedDom2<D0, D1> = DualDom<D0, D1>;
pub type MixedDom3<D0, D1, D2> = DualDom<DualDom<D0, D1>, D2>;
pub type MixedDom4<D0, D1, D2, D3> = DualDom<DualDom<DualDom<D0, D1>, D2>, D3>;

#[derive(Clone, Copy)]
pub enum DualDom<D0, D1> {
    Dom0(D0),
    Dom1(D1),
}

impl<D0: Domain, D1: Domain> Domain for DualDom<D0, D1> {
    fn size(&self, trail: &contrail::Trail) -> usize {
        match self {
            DualDom::Dom0(dom) => dom.size(trail),
            DualDom::Dom1(dom) => dom.size(trail),
        }
    }

    fn contains(&self, trail: &contrail::Trail, val: DomInt) -> bool {
        match self {
            DualDom::Dom0(dom) => dom.contains(trail, val),
            DualDom::Dom1(dom) => dom.contains(trail, val),
        }
    }

    fn val(&self, trail: &contrail::Trail) -> Option<DomInt> {
        match self {
            DualDom::Dom0(dom) => dom.val(trail),
            DualDom::Dom1(dom) => dom.val(trail),
        }
    }

    fn min(&self, trail: &contrail::Trail) -> DomInt {
        match self {
            DualDom::Dom0(dom) => dom.min(trail),
            DualDom::Dom1(dom) => dom.min(trail),
        }
    }

    fn max(&self, trail: &contrail::Trail) -> DomInt {
        match self {
            DualDom::Dom0(dom) => dom.max(trail),
            DualDom::Dom1(dom) => dom.max(trail),
        }
    }

    fn assign(&self, trail: &mut contrail::Trail, val: DomInt) -> Result<Event, ()> {
        match self {
            DualDom::Dom0(dom) => dom.assign(trail, val),
            DualDom::Dom1(dom) => dom.assign(trail, val),
        }
    }

    fn remove(&self, trail: &mut contrail::Trail, val: DomInt) -> Result<Event, ()> {
        match self {
            DualDom::Dom0(dom) => dom.remove(trail, val),
            DualDom::Dom1(dom) => dom.remove(trail, val),
        }
    }

    fn update_min(&self, trail: &mut contrail::Trail, new_min: DomInt) -> Result<Event, ()> {
        match self {
            DualDom::Dom0(dom) => dom.update_min(trail, new_min),
            DualDom::Dom1(dom) => dom.update_min(trail, new_min),
        }
    }

    fn update_max(&self, trail: &mut contrail::Trail, new_max: DomInt) -> Result<Event, ()> {
        match self {
            DualDom::Dom0(dom) => dom.update_max(trail, new_max),
            DualDom::Dom1(dom) => dom.update_max(trail, new_max),
        }
    }
}
