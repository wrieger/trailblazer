use contrail::collections::StableBitSet;
use contrail::collections::TrailedBitSet;
use contrail::Trail;
use contrail::TrailBuilder;

use crate::var::DomInt;
use crate::var::Domain;
use crate::var::Event;

pub struct ZeroOneDomArray {
    n_doms: usize,
    assigned: TrailedBitSet,
    values: StableBitSet,
}

impl ZeroOneDomArray {
    pub fn new(trail: &mut TrailBuilder, n_doms: usize) -> Self {
        Self {
            n_doms,
            assigned: TrailedBitSet::new_empty(trail, n_doms),
            values: StableBitSet::new_empty(trail, n_doms),
        }
    }

    pub fn get(&self, index: usize) -> ZeroOneDom {
        assert!(index < self.n_doms);
        ZeroOneDom {
            index,
            assigned: self.assigned,
            values: self.values,
        }
    }
}

#[derive(Clone, Copy)]
pub struct ZeroOneDom {
    index: usize,
    assigned: TrailedBitSet,
    values: StableBitSet,
}

impl ZeroOneDom {
    #[inline(always)]
    fn state(&self, trail: &Trail) -> Option<bool> {
        if self.assigned.contains(trail, self.index) {
            Some(self.values.contains(trail, self.index))
        } else {
            None
        }
    }

    fn set(&self, trail: &mut Trail, val: bool) {
        self.assigned.remove(trail, self.index);
        if val {
            self.values.insert(trail, self.index);
        } else {
            self.values.remove(trail, self.index);
        }
    }
}

impl Domain for ZeroOneDom {
    fn size(&self, trail: &Trail) -> usize {
        match self.state(trail) {
            None => 2,
            _ => 1,
        }
    }

    fn val(&self, trail: &Trail) -> Option<DomInt> {
        self.state(trail)
            .map(|b| if b { DomInt(1) } else { DomInt(0) })
    }

    fn min(&self, trail: &Trail) -> DomInt {
        match self.state(trail) {
            Some(true) => DomInt(1),
            _ => DomInt(0),
        }
    }

    fn max(&self, trail: &Trail) -> DomInt {
        match self.state(trail) {
            Some(false) => DomInt(0),
            _ => DomInt(1),
        }
    }

    fn contains(&self, trail: &Trail, val: DomInt) -> bool {
        match self.state(trail) {
            Some(false) => val == DomInt(0),
            Some(true) => val == DomInt(1),
            None => val == DomInt(0) || val == DomInt(1),
        }
    }

    fn assign(&self, trail: &mut Trail, val: DomInt) -> Result<Event, ()> {
        match self.state(trail) {
            Some(false) => {
                if val == DomInt(0) {
                    Ok(Event::None)
                } else {
                    Err(())
                }
            }
            Some(true) => {
                if val == DomInt(1) {
                    Ok(Event::None)
                } else {
                    Err(())
                }
            }
            None => {
                if val == DomInt(0) {
                    self.set(trail, false);
                    Ok(Event::Assigned)
                } else if val == DomInt(1) {
                    self.set(trail, true);
                    Ok(Event::Assigned)
                } else {
                    Err(())
                }
            }
        }
    }

    fn remove(&self, trail: &mut Trail, val: DomInt) -> Result<Event, ()> {
        match self.state(trail) {
            Some(false) => {
                if val == DomInt(0) {
                    Err(())
                } else {
                    Ok(Event::None)
                }
            }
            Some(true) => {
                if val == DomInt(1) {
                    Err(())
                } else {
                    Ok(Event::None)
                }
            }
            None => {
                if val == DomInt(0) {
                    self.set(trail, true);
                    Ok(Event::Assigned)
                } else if val == DomInt(1) {
                    self.set(trail, false);
                    Ok(Event::Assigned)
                } else {
                    Ok(Event::None)
                }
            }
        }
    }

    fn update_min(&self, trail: &mut Trail, new_min: DomInt) -> Result<Event, ()> {
        match self.state(trail) {
            Some(false) => {
                if new_min <= DomInt(0) {
                    Ok(Event::None)
                } else {
                    Err(())
                }
            }
            Some(true) => {
                if new_min <= DomInt(1) {
                    Ok(Event::None)
                } else {
                    Err(())
                }
            }
            None => {
                if new_min <= DomInt(0) {
                    Ok(Event::None)
                } else if new_min == DomInt(1) {
                    self.set(trail, true);
                    Ok(Event::Assigned)
                } else {
                    Err(())
                }
            }
        }
    }

    fn update_max(&self, trail: &mut Trail, new_max: DomInt) -> Result<Event, ()> {
        match self.state(trail) {
            Some(false) => {
                if new_max >= DomInt(0) {
                    Ok(Event::None)
                } else {
                    Err(())
                }
            }
            Some(true) => {
                if new_max >= DomInt(1) {
                    Ok(Event::None)
                } else {
                    Err(())
                }
            }
            None => {
                if new_max >= DomInt(1) {
                    Ok(Event::None)
                } else if new_max == DomInt(0) {
                    self.set(trail, false);
                    Ok(Event::Assigned)
                } else {
                    Err(())
                }
            }
        }
    }
}
