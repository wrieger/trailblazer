use std::hash::Hash;

use crate::var::DomInt;

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Monotonicity {
    Monotonic,
    Antimonotonic,
}

impl Monotonicity {
    #[inline(always)]
    fn inverse(self) -> Self {
        match self {
            Monotonicity::Monotonic => Monotonicity::Antimonotonic,
            Monotonicity::Antimonotonic => Monotonicity::Monotonic,
        }
    }
}

pub trait View: Copy + Ord {
    type Val: Copy + Hash + Ord;

    fn monotonicity(&self) -> Monotonicity;
    fn map(&self, val: DomInt) -> Self::Val;
    fn unmap(&self, val: Self::Val) -> Option<DomInt>;
}

#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Base;

impl View for Base {
    type Val = i64;

    #[inline(always)]
    fn monotonicity(&self) -> Monotonicity {
        Monotonicity::Monotonic
    }

    #[inline(always)]
    fn map(&self, val: DomInt) -> i64 {
        val.0
    }

    #[inline(always)]
    fn unmap(&self, val: i64) -> Option<DomInt> {
        Some(DomInt(val))
    }
}

#[derive(Clone, Copy, Eq, Ord, PartialEq, PartialOrd)]
pub struct Offset<V> {
    inner: V,
    offset: i64,
}

impl<V> Offset<V>
where
    V: View,
{
    pub fn new(inner: V, offset: i64) -> Self {
        Self { inner, offset }
    }
}

impl<V> View for Offset<V>
where
    V: View<Val = i64>,
{
    type Val = i64;

    #[inline(always)]
    fn monotonicity(&self) -> Monotonicity {
        self.inner.monotonicity()
    }

    #[inline(always)]
    fn map(&self, val: DomInt) -> i64 {
        self.inner.map(val) + self.offset
    }

    #[inline(always)]
    fn unmap(&self, val: i64) -> Option<DomInt> {
        self.inner.unmap(val - self.offset)
    }
}

#[derive(Clone, Copy, Eq, Ord, PartialEq, PartialOrd)]
pub struct Minus<V> {
    inner: V,
}

impl<V> Minus<V>
where
    V: View,
{
    pub fn new(inner: V) -> Self {
        Self { inner }
    }
}

impl<V> View for Minus<V>
where
    V: View<Val = i64>,
{
    type Val = i64;

    #[inline(always)]
    fn monotonicity(&self) -> Monotonicity {
        self.inner.monotonicity().inverse()
    }

    #[inline(always)]
    fn map(&self, val: DomInt) -> i64 {
        -self.inner.map(val)
    }

    #[inline(always)]
    fn unmap(&self, val: i64) -> Option<DomInt> {
        self.inner.unmap(-val)
    }
}
