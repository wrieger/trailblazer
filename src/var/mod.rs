pub mod dom;
pub mod view;

use std::fmt;

use contrail::Trail;

use crate::propagate::PropId;
use crate::var::view::Base;
use crate::var::view::Minus;
use crate::var::view::Monotonicity;
use crate::var::view::Offset;
use crate::var::view::View;

// use self::dom::SparseSetDom;

/// An integer stored in a domain.
#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct DomInt(pub i64);

pub trait Domain: Copy {
    fn size(&self, trail: &Trail) -> usize;
    fn val(&self, trail: &Trail) -> Option<DomInt>;
    fn min(&self, trail: &Trail) -> DomInt;
    fn max(&self, trail: &Trail) -> DomInt;
    fn contains(&self, trail: &Trail, val: DomInt) -> bool;
    fn assign(&self, trail: &mut Trail, val: DomInt) -> Result<Event, ()>;
    fn remove(&self, trail: &mut Trail, val: DomInt) -> Result<Event, ()>;
    fn update_min(&self, trail: &mut Trail, new_min: DomInt) -> Result<Event, ()>;
    fn update_max(&self, trail: &mut Trail, new_max: DomInt) -> Result<Event, ()>;
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Event {
    Assigned,
    BoundsChanged,
    Reduced,
    None,
}

impl Event {
    pub fn union(self, other: Self) -> Self {
        match (self, other) {
            (Event::Assigned, _) | (_, Event::Assigned) => Event::Assigned,
            (Event::BoundsChanged, _) | (_, Event::BoundsChanged) => Event::BoundsChanged,
            (Event::Reduced, _) | (_, Event::Reduced) => Event::Reduced,
            (Event::None, Event::None) => Event::None,
        }
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Trigger {
    OnAssigned,
    OnBoundsChanged,
    OnReduced,
}

#[derive(Clone, Debug)]
struct Watchers {
    watchers: Vec<PropId>,
    bounds_offset: usize,
    assigned_offset: usize,
}

impl Watchers {
    fn new() -> Self {
        Self {
            watchers: vec![],
            bounds_offset: 0,
            assigned_offset: 0,
        }
    }

    fn watch(&mut self, prop_id: PropId, trigger: Trigger) {
        match trigger {
            Trigger::OnAssigned => {
                self.watchers.push(prop_id);
            }
            Trigger::OnBoundsChanged => {
                self.watchers.insert(self.assigned_offset, prop_id);
                self.assigned_offset += 1;
            }
            Trigger::OnReduced => {
                self.watchers.insert(self.bounds_offset, prop_id);
                self.bounds_offset += 1;
                self.assigned_offset += 1;
            }
        }
    }

    // TODO: Remove this Box
    fn trigger<'s>(&'s self, event: Event) -> impl Iterator<Item = PropId> + 's {
        let to_take = match event {
            Event::None => 0,
            Event::Reduced => self.bounds_offset,
            Event::BoundsChanged => self.assigned_offset,
            Event::Assigned => self.watchers.len(),
        };
        self.watchers.iter().take(to_take).cloned()
    }
}

#[derive(Clone, Debug)]
pub struct VarWatchers {
    watchers: Vec<Watchers>,
}

impl VarWatchers {
    pub fn new(n_vars: usize) -> Self {
        Self {
            watchers: vec![Watchers::new(); n_vars],
        }
    }

    pub fn watch<D, V>(&mut self, prop_id: PropId, var: Var<D, V>, trigger: Trigger)
    where
        D: Domain,
        V: View,
    {
        self.watchers[var.id().id].watch(prop_id, trigger);
    }

    pub fn trigger<'s, D, V>(
        &'s self,
        var: Var<D, V>,
        event: Event,
    ) -> impl Iterator<Item = PropId> + 's
    where
        D: Domain,
        V: View,
    {
        self.watchers[var.id().id].trigger(event)
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub struct VarId {
    id: usize,
}

impl VarId {
    pub fn new(id: usize) -> Self {
        Self { id }
    }
}

#[derive(Clone, Copy)]
pub struct Var<D, V> {
    dom: D,
    view: V,
    id: VarId,
}

impl<D> fmt::Debug for Var<D, Base> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Var({})", self.id.id)
    }
}

impl<D: Domain, V: View> Eq for Var<D, V> {}

impl<D: Domain, V: View> PartialEq for Var<D, V> {
    fn eq(&self, other: &Var<D, V>) -> bool {
        self.id == other.id
    }
}

impl<D: Domain, V: View> Var<D, V> {
    pub fn new(dom: D, view: V, id: VarId) -> Self {
        Self { dom, view, id }
    }

    pub fn map(&self, val: DomInt) -> V::Val {
        self.view.map(val)
    }

    pub fn unmap(&self, val: V::Val) -> Option<DomInt> {
        self.view.unmap(val)
    }

    pub fn strip_view(&self) -> Var<D, Base> {
        Var {
            dom: self.dom,
            view: Base,
            id: self.id,
        }
    }

    pub fn id(&self) -> VarId {
        self.id
    }

    pub fn offset(&self, offset: i64) -> Var<D, Offset<V>>
    where
        V: View<Val = i64>,
    {
        Var {
            dom: self.dom,
            view: Offset::new(self.view, offset),
            id: self.id,
        }
    }

    pub fn minus(&self) -> Var<D, Minus<V>>
    where
        V: View<Val = i64>,
    {
        Var {
            dom: self.dom,
            view: Minus::new(self.view),
            id: self.id,
        }
    }

    pub fn dom_len(&self, trail: &Trail) -> usize {
        self.dom.size(trail)
    }

    pub fn contains(&self, trail: &Trail, val: V::Val) -> bool {
        if let Some(val) = self.view.unmap(val) {
            self.dom.contains(trail, val)
        } else {
            false
        }
    }

    pub fn val(&self, trail: &Trail) -> Option<V::Val> {
        if self.dom.size(&trail) == 1 {
            Some(self.view.map(self.dom.min(&trail)))
        } else {
            None
        }
    }

    pub fn min(&self, trail: &Trail) -> V::Val {
        match self.view.monotonicity() {
            Monotonicity::Monotonic => self.view.map(self.dom.min(&trail)),
            Monotonicity::Antimonotonic => self.view.map(self.dom.max(&trail)),
        }
    }

    pub fn max(&self, trail: &Trail) -> V::Val {
        match self.view.monotonicity() {
            Monotonicity::Monotonic => self.view.map(self.dom.max(&trail)),
            Monotonicity::Antimonotonic => self.view.map(self.dom.min(&trail)),
        }
    }

    pub fn assign(&self, trail: &mut Trail, val: V::Val) -> Result<Event, ()> {
        if let Some(val) = self.view.unmap(val) {
            self.dom.assign(trail, val)
        } else {
            Err(())
        }
    }

    pub fn remove(&self, trail: &mut Trail, val: V::Val) -> Result<Event, ()> {
        if let Some(val) = self.view.unmap(val) {
            self.dom.remove(trail, val)
        } else {
            Ok(Event::None)
        }
    }

    pub fn update_min(&self, trail: &mut Trail, new_min: V::Val) -> Result<Event, ()> {
        if let Some(new_min) = self.view.unmap(new_min) {
            match self.view.monotonicity() {
                Monotonicity::Monotonic => self.dom.update_min(trail, new_min),
                Monotonicity::Antimonotonic => self.dom.update_max(trail, new_min),
            }
        } else {
            Err(())
        }
    }

    pub fn update_max(&self, trail: &mut Trail, new_max: V::Val) -> Result<Event, ()> {
        if let Some(new_max) = self.view.unmap(new_max) {
            match self.view.monotonicity() {
                Monotonicity::Monotonic => self.dom.update_max(trail, new_max),
                Monotonicity::Antimonotonic => self.dom.update_min(trail, new_max),
            }
        } else {
            Err(())
        }
    }
}
