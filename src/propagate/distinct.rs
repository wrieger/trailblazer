use crate::propagate::EventCollector;
use crate::propagate::PropId;
use crate::propagate::PropStatus;
use crate::propagate::Propagate;
use crate::var::view::View;
use crate::var::Domain;
use crate::var::Trigger;
use crate::var::Var;
use crate::var::VarWatchers;

#[derive(Clone)]
pub struct DistinctProp<D, V> {
    prop_id: PropId,
    vars: Vec<Var<D, V>>,
    var_indices: contrail::StableArray<usize>,
    n_vars: contrail::TrailedValue<usize>,
}

impl<D: Domain + 'static, V: View + 'static> DistinctProp<D, V> {
    pub fn new(
        builder: &mut contrail::TrailBuilder,
        vars: impl IntoIterator<Item = Var<D, V>>,
    ) -> Self {
        let vars = vars.into_iter().collect::<Vec<_>>();
        Self {
            prop_id: PropId(0),
            var_indices: contrail::StableArray::new(builder, 0..vars.len()),
            n_vars: contrail::TrailedValue::new(builder, vars.len()),
            vars,
        }
    }
}

impl<D: Domain + 'static, V: View + 'static> Propagate for DistinctProp<D, V>
where
    V::Val: std::fmt::Debug,
{
    type Dom = D;

    fn id(&self) -> PropId {
        self.prop_id
    }

    fn set_id(&mut self, id: PropId) {
        self.prop_id = id;
    }

    fn init(
        &self,
        trail: &mut contrail::Trail,
        _events: &mut EventCollector<D>,
        watchers: &mut VarWatchers,
    ) -> bool {
        for i in (0..self.var_indices.len()).take(self.n_vars.get(trail)) {
            watchers.watch(self.prop_id, self.vars[i], Trigger::OnAssigned);
        }
        true
    }

    fn propagate(
        &self,
        trail: &mut contrail::Trail,
        events: &mut EventCollector<D>,
    ) -> Result<PropStatus, ()> {
        loop {
            let n_vars = self.n_vars.get(trail);

            if n_vars <= 1 {
                return Ok(PropStatus::Entailed);
            }

            let mut found = None;

            for (i, var_index) in (0..self.var_indices.len())
                .take(n_vars)
                .map(|i| (i, self.var_indices.get(trail, i)))
            {
                if let Some(val_to_remove) = self.vars[var_index].val(trail) {
                    found = Some((val_to_remove, i));
                    break;
                }
            }

            if let Some((val_to_remove, i)) = found {
                for j in (0..self.var_indices.len()).take(n_vars).filter(|&j| j != i) {
                    let var_index_to_remove_from = self.var_indices.get(trail, j);
                    let event = self.vars[var_index_to_remove_from].remove(trail, val_to_remove)?;
                    events.add(self.vars[var_index_to_remove_from], event);
                }
                self.var_indices.swap(trail, i, n_vars - 1);
                self.n_vars.set(trail, n_vars - 1);
            } else {
                return Ok(PropStatus::Fixed);
            }
        }
    }
}
