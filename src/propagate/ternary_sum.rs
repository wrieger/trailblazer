use crate::propagate::EventCollector;
use crate::propagate::PropId;
use crate::propagate::PropStatus;
use crate::propagate::Propagate;
use crate::var::view::View;
use crate::var::Domain;
use crate::var::Trigger;
use crate::var::Var;
use crate::var::VarWatchers;

#[derive(Clone)]
pub struct TernarySumNeProp<D, Vx, Vy, Vz> {
    x: Var<D, Vx>,
    y: Var<D, Vy>,
    z: Var<D, Vz>,
    id: PropId,
}

impl<D, Vx, Vy, Vz> TernarySumNeProp<D, Vx, Vy, Vz>
where
    D: Domain + 'static,
    Vx: View<Val = i64> + 'static,
    Vy: View<Val = i64> + 'static,
    Vz: View<Val = i64> + 'static,
{
    pub fn new(x: Var<D, Vx>, y: Var<D, Vy>, z: Var<D, Vz>) -> Self {
        Self {
            x,
            y,
            z,
            id: PropId(0),
        }
    }
}

impl<D, Vx, Vy, Vz> Propagate for TernarySumNeProp<D, Vx, Vy, Vz>
where
    D: Domain + 'static,
    Vx: View<Val = i64> + 'static,
    Vy: View<Val = i64> + 'static,
    Vz: View<Val = i64> + 'static,
{
    type Dom = D;

    fn id(&self) -> PropId {
        self.id
    }

    fn set_id(&mut self, id: PropId) {
        self.id = id;
    }

    fn init(
        &self,
        trail: &mut contrail::Trail,
        events: &mut EventCollector<D>,
        watchers: &mut VarWatchers,
    ) -> bool {
        self.propagate(trail, events).unwrap();
        watchers.watch(self.id, self.x, Trigger::OnAssigned);
        watchers.watch(self.id, self.y, Trigger::OnAssigned);
        watchers.watch(self.id, self.z, Trigger::OnAssigned);
        true
    }

    fn propagate(
        &self,
        trail: &mut contrail::Trail,
        events: &mut EventCollector<D>,
    ) -> Result<PropStatus, ()> {
        match (self.x.val(trail), self.y.val(trail), self.z.val(trail)) {
            (Some(x_val), Some(y_val), _) => {
                events.add(self.z.clone(), self.z.remove(trail, x_val + y_val)?);
                Ok(PropStatus::Entailed)
            }
            (Some(x_val), _, Some(z_val)) => {
                events.add(self.y.clone(), self.y.remove(trail, z_val - x_val)?);
                Ok(PropStatus::Entailed)
            }
            (_, Some(y_val), Some(z_val)) => {
                events.add(self.x.clone(), self.x.remove(trail, z_val - y_val)?);
                Ok(PropStatus::Entailed)
            }
            _ => Ok(PropStatus::Fixed),
        }
    }
}

#[derive(Clone)]
pub struct TernarySumGeProp<D, Vx, Vy, Vz> {
    x: Var<D, Vx>,
    y: Var<D, Vy>,
    z: Var<D, Vz>,
    id: PropId,
}

impl<D, Vx, Vy, Vz> TernarySumGeProp<D, Vx, Vy, Vz>
where
    D: Domain + 'static,
    Vx: View<Val = i64> + 'static,
    Vy: View<Val = i64> + 'static,
    Vz: View<Val = i64> + 'static,
{
    pub fn new(x: Var<D, Vx>, y: Var<D, Vy>, z: Var<D, Vz>) -> Self {
        Self {
            x,
            y,
            z,
            id: PropId(0),
        }
    }
}

impl<D, Vx, Vy, Vz> Propagate for TernarySumGeProp<D, Vx, Vy, Vz>
where
    D: Domain + 'static,
    Vx: View<Val = i64> + 'static,
    Vy: View<Val = i64> + 'static,
    Vz: View<Val = i64> + 'static,
{
    type Dom = D;

    fn id(&self) -> PropId {
        self.id
    }

    fn set_id(&mut self, id: PropId) {
        self.id = id;
    }

    fn init(
        &self,
        trail: &mut contrail::Trail,
        events: &mut EventCollector<D>,
        watchers: &mut VarWatchers,
    ) -> bool {
        self.propagate(trail, events).unwrap();
        watchers.watch(self.id, self.x, Trigger::OnBoundsChanged);
        watchers.watch(self.id, self.y, Trigger::OnBoundsChanged);
        watchers.watch(self.id, self.z, Trigger::OnBoundsChanged);
        true
    }

    fn propagate(
        &self,
        trail: &mut contrail::Trail,
        events: &mut EventCollector<D>,
    ) -> Result<PropStatus, ()> {
        let x_max = self.x.max(trail);
        let y_max = self.y.max(trail);
        let z_min = self.z.min(trail);

        events.add(self.x, self.x.update_min(trail, z_min - y_max)?);
        events.add(self.y, self.y.update_min(trail, z_min - x_max)?);
        events.add(self.z, self.z.update_max(trail, x_max + y_max)?);

        Ok(PropStatus::Fixed)
    }
}
