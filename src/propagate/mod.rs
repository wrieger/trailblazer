// pub mod count;
pub mod distinct;
pub mod ternary_sum;
pub mod unary;

use crate::var::view::Base;
use crate::var::view::View;
use crate::var::Domain;
use crate::var::Event;
use crate::var::Var;
use crate::var::VarWatchers;

#[derive(Clone, Copy, Debug, Default, Eq, PartialEq)]
pub struct PropId(pub(crate) usize);

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum PropStatus {
    Entailed,
    Fixed,
    Unsure,
}

pub trait Propagate {
    type Dom: Domain;

    fn id(&self) -> PropId;
    fn set_id(&mut self, id: PropId);

    fn init(
        &self,
        trail: &mut contrail::Trail,
        events: &mut EventCollector<Self::Dom>,
        watchers: &mut VarWatchers,
    ) -> bool;
    fn propagate(
        &self,
        trail: &mut contrail::Trail,
        events: &mut EventCollector<Self::Dom>,
    ) -> Result<PropStatus, ()>;
}

#[derive(Debug)]
pub struct EventCollector<D> {
    events: Vec<(Var<D, Base>, Event)>,
}

impl<D: Domain> EventCollector<D> {
    pub fn new() -> Self {
        Self { events: Vec::new() }
    }

    fn add<V: View>(&mut self, var: Var<D, V>, event: Event) -> Event {
        if event != Event::None {
            self.events.push((var.strip_view(), event));
        }
        event
    }

    fn drain<'s>(&'s mut self) -> impl Iterator<Item = (Var<D, Base>, Event)> + 's {
        self.events.drain(..)
    }
}

pub struct PropQueue<D> {
    watchers: VarWatchers,
    propagators: Vec<Box<dyn Propagate<Dom = D>>>,
    queue: std::collections::VecDeque<PropId>,
    enqueued: Vec<bool>,
}

impl<D: Domain + 'static> PropQueue<D> {
    pub fn new(watchers: VarWatchers, propagators: Vec<Box<dyn Propagate<Dom = D>>>) -> Self {
        Self {
            watchers,
            queue: std::collections::VecDeque::new(),
            enqueued: vec![false; propagators.len()],
            propagators,
        }
    }

    pub fn clear(&mut self) {
        self.queue.clear();
        self.enqueued = vec![false; self.propagators.len()];
    }

    pub fn enqueue(&mut self, prop_id: PropId) {
        if !self.enqueued[prop_id.0] {
            self.enqueued[prop_id.0] = true;
            self.queue.push_back(prop_id);
        }
    }

    pub fn propagate(
        &mut self,
        trail: &mut contrail::Trail,
        events: &mut EventCollector<D>,
    ) -> Result<(), ()> {
        while let Some(head_id) = self.queue.pop_front() {
            self.enqueued[head_id.0] = false;
            let status = self.propagators[head_id.0].propagate(trail, events)?;
            for (var, event) in events.drain() {
                for prop_id in self.watchers.trigger(var, event).filter(|prop_id| {
                    if status != PropStatus::Unsure {
                        *prop_id != head_id
                    } else {
                        true
                    }
                }) {
                    if !self.enqueued[prop_id.0] {
                        self.enqueued[prop_id.0] = true;
                        self.queue.push_back(prop_id);
                    }
                }
            }
        }
        Ok(())
    }
}
