use crate::propagate::EventCollector;
use crate::propagate::PropId;
use crate::propagate::PropStatus;
use crate::propagate::Propagate;
use crate::var::view::View;
use crate::var::Domain;
use crate::var::Var;
use crate::var::VarWatchers;

pub struct UnaryNeProp<D, V>
where
    V: View,
{
    var: Var<D, V>,
    val: V::Val,
    id: PropId,
}

impl<D, V> UnaryNeProp<D, V>
where
    D: Domain + 'static,
    V: View<Val = i64>,
{
    pub fn new(var: Var<D, V>, val: V::Val) -> Self {
        Self {
            var,
            val,
            id: PropId(0),
        }
    }
}

impl<D, V> Propagate for UnaryNeProp<D, V>
where
    D: Domain + 'static,
    V: View<Val = i64>,
{
    type Dom = D;

    fn id(&self) -> PropId {
        self.id
    }

    fn set_id(&mut self, id: PropId) {
        self.id = id;
    }

    fn init(
        &self,
        trail: &mut contrail::Trail,
        events: &mut EventCollector<Self::Dom>,
        _watchers: &mut VarWatchers,
    ) -> bool {
        self.propagate(trail, events).is_ok()
    }

    fn propagate(
        &self,
        trail: &mut contrail::Trail,
        events: &mut EventCollector<Self::Dom>,
    ) -> Result<PropStatus, ()> {
        events.add(self.var, self.var.update_min(trail, self.val)?);
        Ok(PropStatus::Entailed)
    }
}

pub struct UnaryGeProp<D, V>
where
    V: View,
{
    var: Var<D, V>,
    val: V::Val,
    id: PropId,
}

impl<D, V> UnaryGeProp<D, V>
where
    D: Domain + 'static,
    V: View<Val = i64>,
{
    pub fn new(var: Var<D, V>, val: V::Val) -> Self {
        Self {
            var,
            val,
            id: PropId(0),
        }
    }
}

impl<D, V> Propagate for UnaryGeProp<D, V>
where
    D: Domain + 'static,
    V: View<Val = i64>,
{
    type Dom = D;

    fn id(&self) -> PropId {
        self.id
    }

    fn set_id(&mut self, id: PropId) {
        self.id = id;
    }

    fn init(
        &self,
        trail: &mut contrail::Trail,
        events: &mut EventCollector<Self::Dom>,
        _watchers: &mut VarWatchers,
    ) -> bool {
        self.propagate(trail, events).is_ok()
    }

    fn propagate(
        &self,
        trail: &mut contrail::Trail,
        events: &mut EventCollector<Self::Dom>,
    ) -> Result<PropStatus, ()> {
        events.add(self.var, self.var.update_min(trail, self.val)?);
        Ok(PropStatus::Entailed)
    }
}
