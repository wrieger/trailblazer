use contrail::StoredUsizeArray;
use contrail::Trail;
use contrail::TrailBuilder;
use contrail::TrailedUsize;

use crate::propagate::EventCollector;
use crate::propagate::PropId;
use crate::propagate::PropStatus;
use crate::propagate::Propagate;
use crate::var::view::View;
use crate::var::DomInt;
use crate::var::Domain;
use crate::var::Var;
use crate::var::VarWatchers;

pub struct CountEqProp<D, Vx, Vy>
where
    Vx: View,
{
    vars: Vec<Var<D, Vx>>,
    var_indices: StoredUsizeArray,
    n_vars: TrailedUsize,

    val: Vx::Val,

    count: Var<D, Vy>,

    n_assigned: TrailedUsize,

    id: PropId,
}

impl<D, Vx, Vy> Propagate for CountEqProp<D, Vx, Vy>
where
    D: Domain,
    Vx: View,
    Vy: View<Val = i64>,
{
    type Dom = D;

    fn id(&self) -> PropId {
        self.id
    }

    fn set_id(&mut self, id: PropId) {
        self.id = id
    }

    fn init(
        &self,
        trail: &mut contrail::Trail,
        events: &mut EventCollector<Self::Dom>,
        watchers: &mut VarWatchers,
    ) -> bool {
        unimplemented!()
    }

    fn propagate(
        &self,
        trail: &mut contrail::Trail,
        events: &mut EventCollector<Self::Dom>,
    ) -> Result<PropStatus, ()> {
        for i in (0..self.n_vars.get(trail)).rev() {
            let var = self.vars[i];
            if var.val(trail) == Some(self.val) {
            } else if !var.contains(trail, self.val) {
            }
        }
        unimplemented!()
    }
}
