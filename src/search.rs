use crate::propagate::EventCollector;
use crate::propagate::PropQueue;
use crate::var::view::Base;
use crate::var::Domain;
use crate::var::Event;
use crate::var::Var;
use crate::var::VarWatchers;
// use crate::var::DomInt;
// use crate::var::view::View;

pub trait Decision {
    type Dom: Domain;

    fn apply(&self, trail: &mut contrail::Trail) -> Result<(Var<Self::Dom, Base>, Event), ()>;
}

#[derive(Clone, Copy)]
pub enum EqNeDecision<D> {
    Eq(Var<D, Base>, i64),
    Ne(Var<D, Base>, i64),
}

impl<D: Domain + 'static> Decision for EqNeDecision<D> {
    type Dom = D;

    fn apply(&self, trail: &mut contrail::Trail) -> Result<(Var<D, Base>, Event), ()> {
        match self {
            EqNeDecision::Eq(var, val) => var.assign(trail, *val).map(|e| (*var, e)),
            EqNeDecision::Ne(var, val) => var.remove(trail, *val).map(|e| (*var, e)),
        }
    }
}

#[derive(Clone, Copy)]
pub enum GeLeDecision<D> {
    Ge(Var<D, Base>, i64),
    Le(Var<D, Base>, i64),
}

impl<D: Domain + 'static> Decision for GeLeDecision<D> {
    type Dom = D;

    fn apply(&self, trail: &mut contrail::Trail) -> Result<(Var<D, Base>, Event), ()> {
        match self {
            GeLeDecision::Ge(var, val) => var.update_min(trail, *val).map(|e| (var.clone(), e)),
            GeLeDecision::Le(var, val) => var.update_max(trail, *val).map(|e| (var.clone(), e)),
        }
    }
}

pub trait Heuristic {
    type Dom: Domain;
    type Decision: Decision<Dom = Self::Dom>;

    fn next_decisions(&self, trail: &mut contrail::Trail) -> Vec<Self::Decision>;
}

pub struct StaticMin<D> {
    vars: Vec<Var<D, Base>>,
    n_assigned: contrail::TrailedValue<usize>,
}

impl<D: Domain + 'static> StaticMin<D> {
    pub fn new(builder: &mut contrail::TrailBuilder, vars: Vec<Var<D, Base>>) -> Self {
        Self {
            vars,
            n_assigned: contrail::TrailedValue::new(builder, 0),
        }
    }
}

impl<D: Domain + 'static> Heuristic for StaticMin<D> {
    type Dom = D;
    type Decision = EqNeDecision<D>;

    fn next_decisions(&self, trail: &mut contrail::Trail) -> Vec<EqNeDecision<D>> {
        if let Some((i, &var)) = self
            .vars
            .iter()
            .enumerate()
            .skip(self.n_assigned.get(trail))
            .filter(|(_, var)| var.val(trail).is_none())
            .nth(0)
        {
            self.n_assigned.set(trail, i);
            let val = var.min(trail);
            let eq_decision = EqNeDecision::Eq(var, val);
            let ne_decision = EqNeDecision::Ne(var, val);
            vec![ne_decision, eq_decision]
        } else {
            vec![]
        }
    }
}

pub struct FailFirstMin<D> {
    vars: Vec<Var<D, Base>>,
}

impl<D: Domain> FailFirstMin<D> {
    pub fn new(vars: Vec<Var<D, Base>>) -> Self {
        Self { vars }
    }
}

impl<D: Domain + 'static> Heuristic for FailFirstMin<D> {
    type Dom = D;
    type Decision = EqNeDecision<D>;

    fn next_decisions(&self, trail: &mut contrail::Trail) -> Vec<EqNeDecision<D>> {
        if let Some(var) = self
            .vars
            .iter()
            .filter(|var| var.val(trail).is_none())
            .nth(0)
        {
            let val = var.min(trail);
            let eq_decision = EqNeDecision::Eq(var.clone(), val);
            let ne_decision = EqNeDecision::Ne(var.clone(), val);
            vec![ne_decision, eq_decision]
        } else {
            vec![]
        }
    }
}

pub struct Search<D, F, H>
where
    H: Heuristic,
{
    watchers: VarWatchers,
    decisions: Vec<Vec<H::Decision>>,
    prop_queue: PropQueue<D>,
    heuristic: H,
    on_solution: F,
    explored: usize,
    failures: usize,
}

impl<D, F, H> Search<D, F, H>
where
    D: Domain + 'static,
    F: FnMut(&mut contrail::Trail) -> (),
    H: Heuristic<Dom = D>,
{
    pub fn new(
        watchers: VarWatchers,
        prop_queue: PropQueue<D>,
        heuristic: H,
        on_solution: F,
    ) -> Self {
        Self {
            watchers,
            decisions: vec![],
            prop_queue: prop_queue,
            heuristic,
            on_solution,
            explored: 0,
            failures: 0,
        }
    }

    fn propagate_and_expand(
        &mut self,
        trail: &mut contrail::Trail,
        events: &mut EventCollector<D>,
    ) -> Result<bool, ()> {
        self.prop_queue.propagate(trail, events)?;

        let next_decisions = self.heuristic.next_decisions(trail);
        if next_decisions.is_empty() {
            Ok(false)
        } else {
            self.decisions.push(next_decisions);
            Ok(true)
        }
    }

    pub fn search(&mut self, trail: &mut contrail::Trail) {
        let mut events = EventCollector::new();

        match self.propagate_and_expand(trail, &mut events) {
            Ok(false) | Err(()) => return,
            _ => {}
        }

        while let Some(mut decisions) = self.decisions.pop() {
            if let Some(decision) = decisions.pop() {
                self.decisions.push(decisions);
                trail.new_level();

                self.explored += 1;

                match decision.apply(trail) {
                    Ok((var, event)) => {
                        for prop in self.watchers.trigger(var, event) {
                            self.prop_queue.enqueue(prop);
                        }
                        match self.propagate_and_expand(trail, &mut events) {
                            Ok(false) => {
                                // solution found
                                (self.on_solution)(trail);
                                trail.undo_level();
                            }
                            Ok(true) => {}
                            Err(()) => {
                                self.prop_queue.clear();
                                self.failures += 1;
                                trail.undo_level();
                            }
                        }
                    }
                    Err(()) => unreachable!(),
                }
            } else {
                trail.undo_level();
            }
        }
        println!("{} {}", self.explored, self.failures);
    }

    pub fn minimize(&mut self, trail: &mut contrail::Trail, to_minimize: Var<D, Base>) {
        let mut events = EventCollector::new();
        let mut min_so_far = None;

        match self.propagate_and_expand(trail, &mut events) {
            Ok(false) | Err(()) => return,
            _ => {}
        }

        while let Some(mut decisions) = self.decisions.pop() {
            if let Some(decision) = decisions.pop() {
                self.decisions.push(decisions);
                trail.new_level();

                self.explored += 1;

                if let Some(current_min) = min_so_far {
                    let dec = GeLeDecision::Le(to_minimize.clone(), current_min - 1);

                    match dec.apply(trail) {
                        Ok((var, event)) => {
                            for prop in self.watchers.trigger(var, event) {
                                self.prop_queue.enqueue(prop);
                            }
                        }
                        Err(()) => {
                            trail.undo_level();
                            self.failures += 1;
                            continue;
                        }
                    }
                }

                match decision.apply(trail) {
                    Ok((var, event)) => {
                        for prop in self.watchers.trigger(var, event) {
                            self.prop_queue.enqueue(prop);
                        }
                        match self.propagate_and_expand(trail, &mut events) {
                            Ok(false) => {
                                // solution found
                                println!("{} {}", self.explored, self.failures);
                                min_so_far = to_minimize.val(trail);
                                trail.undo_level();
                            }
                            Ok(true) => {}
                            Err(()) => {
                                self.failures += 1;
                                trail.undo_level();
                            }
                        }
                    }
                    Err(()) => {
                        self.failures += 1;
                        trail.undo_level();
                    }
                }
            } else {
                trail.undo_level();
            }
        }
        println!("{} {}", self.explored, self.failures);
    }
}
