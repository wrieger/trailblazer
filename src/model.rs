use crate::propagate::distinct::DistinctProp;
use crate::propagate::ternary_sum::TernarySumGeProp;
use crate::propagate::ternary_sum::TernarySumNeProp;
use crate::propagate::EventCollector;
use crate::propagate::PropId;
use crate::propagate::PropQueue;
use crate::propagate::Propagate;
use crate::search::Heuristic;
use crate::search::Search;
use crate::var::view::Base;
use crate::var::view::View;
use crate::var::Domain;
use crate::var::Var;
use crate::var::VarId;
use crate::var::VarWatchers;

pub struct Model<D> {
    next_var_id: usize,
    props: Vec<Box<dyn Propagate<Dom = D>>>,
    phantom: std::marker::PhantomData<D>,
}

impl<D: Domain + 'static> Model<D> {
    pub fn new() -> Self {
        Self {
            next_var_id: 0,
            props: vec![],
            phantom: std::marker::PhantomData,
        }
    }

    pub fn create_var(&mut self, dom: D) -> Var<D, Base> {
        let var = Var::new(dom, Base, VarId::new(self.next_var_id));
        self.next_var_id += 1;
        var
    }

    pub fn eq<Vx, Vy, Vz>(&mut self, x: Var<D, Vx>, y: Var<D, Vy>, z: Var<D, Vz>) -> bool
    where
        Vx: View<Val = i64> + 'static,
        Vy: View<Val = i64> + 'static,
        Vz: View<Val = i64> + 'static,
    {
        self.ge(x, y, z) && self.le(x, y, z)
    }

    pub fn ne<Vx, Vy, Vz>(&mut self, x: Var<D, Vx>, y: Var<D, Vy>, z: Var<D, Vz>) -> bool
    where
        Vx: View<Val = i64> + 'static,
        Vy: View<Val = i64> + 'static,
        Vz: View<Val = i64> + 'static,
    {
        let mut prop = TernarySumNeProp::new(x, y, z);
        prop.set_id(PropId(self.props.len()));
        self.props.push(Box::new(prop));
        true
    }

    pub fn ge<Vx, Vy, Vz>(&mut self, x: Var<D, Vx>, y: Var<D, Vy>, z: Var<D, Vz>) -> bool
    where
        Vx: View<Val = i64> + 'static,
        Vy: View<Val = i64> + 'static,
        Vz: View<Val = i64> + 'static,
    {
        let mut prop = TernarySumGeProp::new(x, y, z);
        prop.set_id(PropId(self.props.len()));
        self.props.push(Box::new(prop));
        true
    }

    pub fn le<Vx, Vy, Vz>(&mut self, x: Var<D, Vx>, y: Var<D, Vy>, z: Var<D, Vz>) -> bool
    where
        Vx: View<Val = i64> + 'static,
        Vy: View<Val = i64> + 'static,
        Vz: View<Val = i64> + 'static,
    {
        let mut prop = TernarySumGeProp::new(x.minus(), y.minus(), z.minus());
        prop.set_id(PropId(self.props.len()));
        self.props.push(Box::new(prop));
        true
    }

    pub fn distinct<I, V>(&mut self, builder: &mut contrail::TrailBuilder, vars: I) -> bool
    where
        I: IntoIterator<Item = Var<D, V>>,
        V: View + 'static,
        // TODO remove this debug
        V::Val: std::fmt::Debug,
    {
        let mut prop = DistinctProp::new(builder, vars.into_iter());
        prop.set_id(PropId(self.props.len()));
        self.props.push(Box::new(prop));
        true
    }

    pub fn search<F, H>(self, trail: &mut contrail::Trail, heuristic: H, on_solution: F)
    where
        F: FnMut(&mut contrail::Trail) -> (),
        H: Heuristic<Dom = D>,
    {
        let mut events = EventCollector::new();
        let mut watchers = VarWatchers::new(self.next_var_id);
        for prop in &self.props {
            prop.init(trail, &mut events, &mut watchers);
        }
        let prop_queue = PropQueue::new(watchers.clone(), self.props);
        let mut search = Search::new(watchers, prop_queue, heuristic, on_solution);
        search.search(trail);
    }

    pub fn minimize<F, H>(
        self,
        trail: &mut contrail::Trail,
        var: Var<D, Base>,
        heuristic: H,
        on_solution: F,
    ) where
        F: FnMut(&mut contrail::Trail) -> (),
        H: Heuristic<Dom = D>,
    {
        let mut events = EventCollector::new();
        let mut watchers = VarWatchers::new(self.next_var_id);
        for prop in &self.props {
            prop.init(trail, &mut events, &mut watchers);
        }
        let prop_queue = PropQueue::new(watchers.clone(), self.props);
        // for (var, event) in events.into_iter() {
        //     for prop_id in watchers.trigger(var, event) {
        //         prop_queue.enqueue(prop_id);
        //     }
        // }
        let mut search = Search::new(watchers, prop_queue, heuristic, on_solution);
        search.minimize(trail, var);
    }
}
