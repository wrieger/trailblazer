extern crate trailblazer;

use trailblazer::var::dom::BitSetDom;
use trailblazer::var::DomInt;

fn main() {
    let mut builder = contrail::TrailBuilder::new();

    let n = std::env::args()
        .nth(1)
        .and_then(|n_str| n_str.parse().ok())
        .unwrap_or(8);

    let mut model = trailblazer::model::Model::new();
    let mut vars = Vec::with_capacity(n);

    for _ in 0..n {
        let dom = BitSetDom::from_bounds(&mut builder, DomInt(0), DomInt(n as i64 - 1));
        vars.push(model.create_var(dom));
    }
    model.distinct(&mut builder, vars.clone());
    model.distinct(
        &mut builder,
        vars.iter().enumerate().map(|(i, var)| var.offset(i as i64)),
    );
    model.distinct(
        &mut builder,
        vars.iter()
            .enumerate()
            .map(|(i, var)| var.offset(-(i as i64))),
    );

    let heuristic = trailblazer::search::FailFirstMin::new(vars.clone());

    let mut trail = builder.finish();

    let mut count = 0;
    model.search(&mut trail, heuristic, |_trail: &mut contrail::Trail| {
        // for var in &vars {
        //     let front = std::iter::repeat(". ")
        //         .take(var.min(trail) as usize)
        //         .collect::<String>();
        //     let back = std::iter::repeat(". ")
        //         .take(n - var.min(trail) as usize - 1)
        //         .collect::<String>();
        //     print!("{}Q {}", front, back);
        //     println!();
        // }
        // println!();
        // println!();
        count += 1;
    });
    println!("{}", count);
}
