use trailblazer::var::dom::mixed2_0;
use trailblazer::var::dom::mixed2_1;
use trailblazer::var::dom::BitSetDom;
use trailblazer::var::dom::ConstantDom;
use trailblazer::var::DomInt;

fn main() {
    let n = std::env::args()
        .nth(1)
        .and_then(|n_str| n_str.parse().ok())
        .unwrap_or(8);

    let mut builder = contrail::TrailBuilder::new();
    let mut model = trailblazer::model::Model::new();
    let mut marks = Vec::with_capacity(n);
    for _ in 0..n {
        let dom = mixed2_0(BitSetDom::from_bounds(
            &mut builder,
            trailblazer::var::DomInt(0),
            trailblazer::var::DomInt((n * n) as i64 - 1),
        ));
        marks.push(model.create_var(dom));
    }
    let zero = model.create_var(mixed2_1(ConstantDom::new(DomInt(0))));
    model.eq(marks[0], zero, zero);
    let mut diffs = Vec::with_capacity(n * (n - 1) / 2);
    for i in 0..n {
        for j in (i + 1)..n {
            let dom = mixed2_0(trailblazer::var::dom::BitSetDom::from_bounds(
                &mut builder,
                trailblazer::var::DomInt(((j - i) * (j - i + 1) / 2) as i64),
                trailblazer::var::DomInt((n * n) as i64),
            ));
            let diff = model.create_var(dom);
            model.eq(marks[i], diff, marks[j]);
            let constant = model.create_var(mixed2_1(ConstantDom::new(DomInt(((n - 1 - j + i) * (n - j + i) / 2) as i64))));
            model.le(diff, constant, marks[n - 1]);
            diffs.push(diff);
        }
    }
    model.le(diffs[0], zero, diffs[n * (n - 1) / 2 - 1]);
    model.distinct(&mut builder, diffs.clone());
    let heuristic = trailblazer::search::StaticMin::new(&mut builder, marks.clone());

    let mut trail = builder.finish();

    model.minimize(
        &mut trail,
        marks[n - 1],
        heuristic,
        |trail: &mut contrail::Trail| {
            println!(
                "{:?}",
                marks
                    .clone()
                    .into_iter()
                    .map(|var| var.val(trail).unwrap())
                    .collect::<Vec<_>>()
            );
        },
    );
}
