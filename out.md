| Command | Mean [ms] | Min…Max [ms] |
|:---|---:|---:|
| `/home/billy/projects/trailblazer/target/release/examples/n_queens 8` | 1.0 ± 0.0 | 0.9…1.1 |
| `/home/billy/or-tools/bin/nqueens2 --size=8` | 12.3 ± 0.3 | 11.8…13.2 |
| `/home/billy/projects/trailblazer/target/release/examples/n_queens 9` | 2.6 ± 0.0 | 2.5…3.5 |
| `/home/billy/or-tools/bin/nqueens2 --size=9` | 15.2 ± 0.4 | 14.6…17.7 |
| `/home/billy/projects/trailblazer/target/release/examples/n_queens 10` | 9.5 ± 0.3 | 9.3…12.7 |
| `/home/billy/or-tools/bin/nqueens2 --size=10` | 27.0 ± 1.9 | 25.6…44.7 |
| `/home/billy/projects/trailblazer/target/release/examples/n_queens 11` | 42.3 ± 0.2 | 42.0…43.4 |
| `/home/billy/or-tools/bin/nqueens2 --size=11` | 82.6 ± 2.3 | 80.7…88.9 |
| `/home/billy/projects/trailblazer/target/release/examples/n_queens 12` | 210.6 ± 0.9 | 209.7…212.8 |
| `/home/billy/or-tools/bin/nqueens2 --size=12` | 383.9 ± 10.2 | 377.3…408.9 |
